;****************************************
; Documentation
;****************************************

; Description:
; A drush makefile for a Launch profile.

;****************************************
; General
;****************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;****************************************
; Launch profile dependencies
;****************************************

; Libraries
libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-6.x-2.0-beta2.tar.gz"
libraries[jquery_ui][download][type] = "get"
libraries[jquery_ui][destination] = "modules/jquery_ui"
libraries[jquery_ui][download][url] = "http://jquery-ui.googlecode.com/files/jquery.ui-1.6.zip"
libraries[jquery_ui][directory_name] = "jquery.ui"

; Features stack
projects[features][destination] = profiles/launch/modules/contrib
projects[features_uuid][destination] = profiles/launch/modules/contrib
projects[uuid][destination] = profiles/launch/modules/contrib
projects[strongarm][destination] = profiles/launch/modules/contrib
projects[ctools][destination] = profiles/launch/modules/contrib
projects[context][destination] = profiles/launch/modules/contrib
projects[boxes][destination] = profiles/launch/modules/contrib
projects[diff][destination] = profiles/launch/modules/contrib

; Utilities
projects[admin_menu][destination] = profiles/launch/modules/contrib
projects[captcha][destination] = profiles/launch/modules/contrib
projects[recaptcha][destination] = profiles/launch/modules/contrib
projects[pathauto][destination] = profiles/launch/modules/contrib
projects[token][destination] = profiles/launch/modules/contrib
projects[jquery_ui][destination] = profiles/launch/modules/contrib

; Webform
projects[webform][destination] = profiles/launch/modules/contrib
projects[options_element][destination] = profiles/launch/modules/contrib
projects[select_or_other][destination] = profiles/launch/modules/contrib
projects[webform_validation][destination] = profiles/launch/modules/contrib
projects[date][destination] = profiles/launch/modules/contrib
projects[arrange_fields][destination] = profiles/launch/modules/contrib
projects[webform_report][destination] = profiles/launch/modules/contrib
projects[webform-fivestar][destination] = profiles/launch/modules/contrib
projects[webform_share][destination] = profiles/launch/modules/contrib
projects[webform_rules][destination] = profiles/launch/modules/contrib
projects[webform_conditional][destination] = profiles/launch/modules/contrib


; Theme
projects[singular][destination] = profiles/launch/themes/contrib
projects[singular][location] = "http://code.developmentseed.org/fserver"

;****************************************
; Modules to overwrite version
;****************************************

;projects[devel][destination] = profiles/openatrium/modules/developer
;projects[devel][version] = 1.25

;****************************************
; End
;****************************************
